/**
 * This typings file contains the types used in the frontend. Note that not all properties that are available in the backend are included here, only the ones used on the frontend.
 */

// Types available from backend
type UserType = {
  id: string
  username: string
  email: string
  contacts: ContactType[]
  matches: MatchType
  profile: ProfileType
}

type ContactType = {
  seen: boolean
  messages?: MessageType[]
  withUser: UserType
  status: ContactStatusEnum
}

enum ContactStatusEnum {
  liked = "liked",
  matched = "matched",
}

type MessageType = {
  seen: boolean
  message: string
  from: string, //is user.id
  to: string, //is user.id
  published_at: string
}


//FIXME: NOTE that User.matched -> PositionMatch is designed to be one-to-one; however Strapi API generation results into this (because .matched -> TalentMatch *is* an array) so this is used as a workaround for now.
type MatchType = PositionMatchType[] | TalentMatchType[]

type PositionMatchType = {
  id: string
  filtered: PositionProfileType[]
  liked: PositionProfileType[]
  matched: PositionProfileType[]
  ignored: PositionProfileType[]
  owner: { id: string } // User.id
}

type PositionProfileType = {
  id: string
  name: string
  description?: string
  location?: string
  requiredExperience?: number
  requiredSkills?: Skill[]
  img?: string //base64 data
  jobType?: JobTypeEnum
  owner: EmployerProfile
}

enum JobTypeEnum {
  fulltime = "fulltime",
  parttime = "parttime",
  freelance = "freelance",
}

type TalentMatchType = {
  id: string
  forPosition: PositionProfileType
  filtered: TalentProfileType[]
  liked: TalentProfileType[]
  matched: TalentProfileType[]
  ignored: TalentProfileType[]
  owner: { id: string } // User.id
}

type TalentProfileType = {
  id: string
  owner: User
  shortDescription?: string
  location?: string
  workExperience?: number
  jobTypeInterest?: JobTypeEnum
  skills?: Skill[]
  img?: string //base64 data
  description?: string
  draft: boolean
}

//FIXME: NOTE that User.profile -> TalentProfile is designed to be one-to-one; however Strapi API generation results into this (because .profile -> EmployerProfile *is* an array) so this is used as a workaround for now.
type ProfileType = EmployerProfileType[] | TalentProfileType[]

type EmployerProfileType = {
  id: string
  owners: User[]
  companyName?: string
  positions?: PositionProfileType[]
  img?: string //base64 data
  draft: boolean
}

type SkillType = {
  id: string
  skillName: string
}

type ReduxStateType =
  | undefined
  | {
      editableEmployerProfile: EmployerProfileType
      editablePositionProfile: PositionProfileType
      editableTalentProfile: TalentProfileType
    }
