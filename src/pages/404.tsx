import React from "react"

import Layout from "../components/Layout"
import SEO from "../components/Seo"

interface NotFoundPage {
  location: {
    pathname: string
  }
}

const NotFoundPage = ({
  location: { pathname },
}: NotFoundPage): JSX.Element => (
  <Layout>
    <SEO title="404: Not found" />
    <p style={{ textAlign: "center" }}>
      You just hit a route that doesn&#39;t exist...{" "}
      {(pathname.startsWith("/profile") || pathname.startsWith("/home")) &&
        "this is probably due to the fact that we're still generating this page for you! Please check back again in a minute."}
    </p>
  </Layout>
)

export default NotFoundPage
