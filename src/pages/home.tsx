import React, { useState, useEffect } from "react"
import { navigate } from "gatsby"
import CircularProgress from "@material-ui/core/CircularProgress"

import Layout from "../components/Layout"
import {
  getFilteredTalentProfiles,
  getFilteredPositionProfiles,
  getUserRole,
} from "../services/dataQueryHandlers"
import { isTalentMatchType } from "../type-guards"

export default function Home(): JSX.Element {
  const [matches, setMatches] = useState<TalentMatchType | PositionMatchType>()

  useEffect(() => {
    // Get filtered matches from backend server at run-time
    getUserRole().then((role: string) => {
      if (role === "talent") getFilteredPositionProfiles().then(setMatches)
      if (role === "employer") getFilteredTalentProfiles().then(setMatches)
    })
  }, [])

  // Redirect to the profile page of the 1st match
  const redirectToFirst = () => {
    navigate(`/home/${matches?.filtered[0]?.id}`, {
      state: { browsing: true, matches: matches },
    })
  }
  //FIXME after navigate, browser back should point to what page?

  return (
    <>
      {matches === undefined ? (
        <div className="pages-home">
          <CircularProgress color="inherit" style={{ alignSelf: "center" }} />
        </div>
      ) : matches.filtered.length ? (
        redirectToFirst()
      ) : (
        <Layout menu="home" className="pages-home">
          {isTalentMatchType(matches) && matches.forPosition === null ? (
            <>
              <h1>No matches found yet.</h1>
              <h1>Please create your first position and come back later...</h1>
            </>
          ) : (
            <>
              <h1>No more matches found.</h1>
              <h1>Please come back later...</h1>
            </>
          )}
        </Layout>
      )}
    </>
  )
}
