import React, { useEffect } from "react"

import Layout from "../components/Layout"
import PositionProfile from "../components/PositionProfile"

interface CreatePosition {
  location?: {
    state: {
      owner: string
    }
  }
}

/**
 * @param {string} location.state.owner EmployerProfile.id to be used as the owner of this position
 * @returns
 */
export default function CreatePosition({
  location,
}: CreatePosition): JSX.Element {
  const dataPlaceholders = {
    id: "",
    name: "Position title...",
    description: "Position description...",
    location: "Position location...",
    requiredExperience: 0,
    img: "base64 data",
    jobType: "fulltime" as JobTypeEnum,
    requiredSkills: [],
    owner: "",
  }

  useEffect(() => {
    dataPlaceholders.owner = String(location?.state.owner)
  }, [location])

  return (
    <Layout menu="profile" className="pages-profile">
      <PositionProfile data={dataPlaceholders} editable={true} />
    </Layout>
  )
}
