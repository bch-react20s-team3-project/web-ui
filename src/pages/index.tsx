import React, { useEffect } from "react"
import { navigate } from "gatsby"
import CircularProgress from "@material-ui/core/CircularProgress"

export default function IndexPage(): JSX.Element {
  useEffect(() => {
    navigate("/get-started")
  }, [])
  return (
    <div className="pages-index">
      <CircularProgress color="inherit" style={{ alignSelf: "center" }} />
    </div>
  )
}
