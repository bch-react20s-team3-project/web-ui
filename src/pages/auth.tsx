import React, { useState, useEffect } from "react"
import { navigate } from "gatsby"

import Layout from "../components/Layout"
import { Input } from "@material-ui/core"
import { login } from "../services/dataMutationHandlers"

export default function Auth(): JSX.Element {
  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")

  useEffect(() => {
    const jwtExpiration = localStorage.getItem("jwt-expires")
    if (Number(jwtExpiration) > Date.now()) navigate(`/home`)
  }, [])

  const handleLogin = () => {
    login({ email, password })
      .then(({ jwt, id }) => {
        // Save token and user id to local storage
        localStorage.setItem("jwt", jwt)
        localStorage.setItem("jwt-expires", String(Date.now() + 1000 * 60 * 60))
        localStorage.setItem("userId", id)

        // Move to the home view
        navigate(`/home`)
      })
      .catch(alert)
  }

  return (
    <Layout nav={false} className="pages-auth">
      <h1>Please enter your credentials</h1>
      <Input
        type="text"
        placeholder="Enter email"
        onChange={e => setEmail(e.target.value)}
        value={email}
      />
      <Input
        type="password"
        placeholder="Enter password"
        onChange={e => setPassword(e.target.value)}
        value={password}
      />
      <button onClick={handleLogin}>Continue</button>
    </Layout>
  )
}
