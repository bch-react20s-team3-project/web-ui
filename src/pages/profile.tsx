import React, { useEffect } from "react"
import { navigate } from "gatsby"
import { CircularProgress } from "@material-ui/core"

import { getUserProfileId } from "../services/dataQueryHandlers"

export default function ProfilePage(): JSX.Element {
  useEffect(() => {
    getUserProfileId().then((profileId: string) =>
      navigate(`/profile/${profileId}`)
    )
  }, [])
  return (
    <div className="pages-profile">
      <CircularProgress
        color="inherit"
        style={{ marginTop: "2em", alignSelf: "center" }}
      />
    </div>
  )
}
