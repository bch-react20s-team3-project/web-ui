import React, { useState, useEffect } from "react"
import { Checkbox, CircularProgress, Input } from "@material-ui/core"
import { navigate } from "gatsby"
import Alert from "@material-ui/lab/Alert"
import { makeStyles, createStyles } from "@material-ui/core/styles"

import Layout from "../components/Layout"
import {
  register,
  createEmployerProfile,
  createTalentProfile,
  createPositionMatches,
  createTalentMatches,
} from "../services/dataMutationHandlers"

const useStyles = makeStyles(() =>
  createStyles({
    alert: {
      width: "100%",
      margin: "1em 0",
    },
  })
)

interface Register {
  location: {
    state?: {
      desiredRole: "talent" | "employer"
    }
  }
}

export default function Register({
  location: { state },
}: Register): JSX.Element {
  const [agree, setAgree] = useState(false)
  const [showAlert, setShowAlert] = useState(false)
  const [continueWithEmail, setContinueWithEmail] = useState(false)
  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")
  const [desiredRole, setDesiredRole] = useState<"talent" | "employer">(
    "talent"
  )
  const [loading, setLoading] = useState(false)

  const classes = useStyles()

  useEffect(() => {
    setDesiredRole(state?.desiredRole || "talent")
  }, [])

  const handleRegister = () => {
    setLoading(true)

    // Call backend to register the new user
    register({
      username: email.substr(0, email.indexOf("@")), //TODO add a separate input for username instead
      email: email,
      password: password,
      desiredRole: desiredRole,
    })
      .then(async ({ jwt, id }) => {
        // Save token and user id to local storage
        localStorage.setItem("jwt", jwt)
        localStorage.setItem(
          "jwt-expires",
          String(Date.now() + 1000 * 60 * 60 * 24)
        )
        localStorage.setItem("userId", id)

        // Create placeholder profile/matches (must be done in sequence with profile creation as both make updates to the user) and move back to login page
        if (desiredRole === "talent")
          createTalentProfile(id)
            .then(() => createPositionMatches(id))
            .then(() => navigate(`/login`, { state: { isAuth: true } }))
        else if (desiredRole === "employer")
          createEmployerProfile(id)
            .then(() => createTalentMatches(id))
            .then(() => navigate(`/login`, { state: { isAuth: true } }))
      })
      .catch(msg => {
        setLoading(false)
        alert(msg)
      })
  }

  return (
    <Layout nav={false} className="pages-register">
      <span>
        You are one step away from creating wonderful career connections and
        opportunities
      </span>
      <div>
        <Checkbox
          onChange={() => {
            setAgree(!agree)
            if (continueWithEmail) setContinueWithEmail(!continueWithEmail)
          }}
          value={agree}
        />
        <span>I accept the Privacy Policy and Terms and Conditions of Use</span>
      </div>
      <>
        {
          !showAlert && !continueWithEmail && (
            <button
              onClick={() =>
                agree ? setContinueWithEmail(true) : setShowAlert(true)
              }
            >
              Continue with email
            </button>
          )
          //TODO additional 3rd party auth providers
          //<button disabled>Continue with Google</button>
          //<button disabled>Continue with Apple</button>
        }

        {
          continueWithEmail && (
            <>
              <Input
                type="text"
                placeholder="Enter email"
                onChange={e => setEmail(e.target.value)}
                value={email}
              />
              <Input
                type="password"
                placeholder="Enter password"
                onChange={e => setPassword(e.target.value)}
                value={password}
              />
              {loading ? (
                <button>
                  <CircularProgress
                    size={16}
                    color="inherit"
                    style={{ alignSelf: "center" }}
                  />
                </button>
              ) : (
                <button onClick={handleRegister}>Register</button>
              )}
            </>
          )
          //TODO inputs for email and password validation (to check that they match with no typos)
        }

        {showAlert && (
          <Alert
            className={classes.alert}
            severity="warning"
            onClose={() => setShowAlert(false)}
          >
            Please accept to continue registering
          </Alert>
        )}
      </>
    </Layout>
  )
}
