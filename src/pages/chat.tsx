import React, { useState, useEffect, useRef } from "react"

import { socket } from "../../config/web-sockets"
import Avatar from "../components/Avatar"
import Layout from "../components/Layout"

interface Chat {
  location: {
    state: {
      id: string
      name: string
      email: string
      img: string
    }
  }
}

/**
 * @param location Contact data with whom to chat.
 * @returns JSX.Element
 */
export default function Chat({ location: { state } }: Chat): JSX.Element {
  const [userId, setUserId] = useState<string>("")
  const [contactId, setContactId] = useState<string>("")
  const [messages, setMessages] = useState<MessageType[]>([] as MessageType[])
  const [newMessage, setNewMessage] = useState<string>("")
  const [img, setImg] = useState<string>()
  const [socketConnect, setSocketConnect] = useState(false)
  const [contactRoom, setContactRoom] = useState<string>()
  const messagesEndRef = useRef<HTMLDivElement>(null)

  const scrollToBottom = () => {
    messagesEndRef.current?.scrollIntoView({ behavior: "smooth" })
  }

  const addNewMessage = (message: MessageType) => {
    setMessages(messages => [...messages, message])
  }

  useEffect(() => {
    scrollToBottom()

    if (!socketConnect) {
      const userId = String(localStorage.getItem("userId"))
      setUserId(userId)
      setContactId(state.id)
      setContactRoom(state.id + userId)
      setImg(state.img)
      socket.connect()
      socket.emit("join", {
        me: userId,
        contact: state.id,
        room: userId + state.id,
      })
      setSocketConnect(true)
      socket.on("get message history", setMessages)
      socket.on("new message", (message: MessageType) => addNewMessage(message))
    }

    //TODO socket.io on presence handler
    // if (location) setContactId(location.state.contactId)
    // contact();
    return () => {
      //FIXME @Janne #breaks socket.io connection with msg scrolling related useRef, might lead into memory leaks if we don't explicitly call disconnect
      //socket.disconnect()
    }
  }, [messages])

  const handleChange = (e: React.ChangeEvent<HTMLTextAreaElement>) =>
    setNewMessage(e.target.value)

  const handleEnter = (e: React.KeyboardEvent<HTMLTextAreaElement>) => {
    if (e.which === 13 && !e.shiftKey) {
      e.target.blur()
      const message = {
        published_at: new Date(Date.now()).toISOString(),
        seen: true,
        message: newMessage,
        to: contactId,
        from: userId,
      }

      addNewMessage(message)

      socket.emit("push new message", message, contactRoom)
      setNewMessage("")
    }
  }

  return (
    <Layout menu="matches" className="pages-chat">
      <div className="chat-contact-details">
        {/* TODO @Janne #blocking-contacts add a button for "Block user" */}
        <div className="chat-avatar">
          <Avatar size="large" src={String(img)} />
        </div>
        <p>
          {state?.name || "Name N/A"}
        </p>
        <p>
          {state?.email || "Email N/A"}
        </p>
      </div>
      <div className="chat-messages">
        <ul>
          {messages.map(message => (
            <div
              key={message.published_at}
              className={`message-container ${
                message.to === contactId ? "outbound" : "inbound"
              }`}
            >
              <li className="message-time-stamp">
                {new Date(message.published_at).toLocaleTimeString()}
              </li>
              <li>{message.message}</li>
            </div>
          ))}
          <div ref={messagesEndRef} />
        </ul>
      </div>
      <div className="input-wrapper">
        <textarea
          name="newMessage"
          value={newMessage}
          onChange={handleChange}
          onKeyPress={handleEnter}
          className="chat-input"
          placeholder="Type a new message..."
        />
      </div>
    </Layout>
  )
}
