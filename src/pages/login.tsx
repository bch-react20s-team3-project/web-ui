import React, { useEffect, useState } from "react"
import { navigate, Link } from "gatsby"

import SEO from "../components/Seo"
import Button from "../components/Button"
import Layout from "../components/Layout"

interface Login {
  location?: {
    state: {
      isAuth: boolean
    }
  }
}

export default function Login({ location }: Login): JSX.Element {
  const [isAuth, setIsAuth] = useState(false)

  useEffect(() => {
    setIsAuth(location?.state?.isAuth || false)
  }, [])

  return (
    <Layout nav={false} className="pages-login">
      <SEO title="Login" />
      <p>Welcome to</p>
      <h1 className="logo">Jobvious...</h1>
      <>
        {isAuth && (
          <p>
            Jobvious is the easiest and quickest way to connect tech talent and
            working opportunities
          </p>
        )}
      </>
      <div className="pages-login-buttons">
        <p>
          {isAuth ? "Ready to explore?" : "To register select the following:"}
        </p>
        {isAuth ? (
          <Button
            onClick={() => navigate(`/home`)}
            buttonName="Start browsing!"
          />
        ) : (
          <>
            <Button
              onClick={() =>
                navigate("/register", {
                  state: { desiredRole: "talent" },
                })
              }
              buttonName="I am a Talent"
            />
            <Button
              onClick={() =>
                navigate("/register", {
                  state: { desiredRole: "employer" },
                })
              }
              buttonName="I am an Employer"
            />
          </>
        )}
        {!isAuth && <Link to="/auth">I already have an account</Link>}
      </div>
    </Layout>
  )
}
