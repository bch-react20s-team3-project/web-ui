import React from "react"
import { navigate } from "gatsby"

import Button from "../components/Button"
import SEO from "../components/Seo"
import Layout from "../components/Layout"

export default function GetStarted(): JSX.Element {
  //TODO add a Gatsby useStaticQuery for the phrases (see pages/chat.tsx for example)

  return (
    <Layout nav={false} className="pages-get-started">
      <SEO title="Get started" />
      <h1 className="logo">Jobvious...</h1>
      {/* TODO move content values to an external strapi (Phrases: { id: ... , text: string }) */}
      <p className="bolder-body-text">
        Automatically turn your opportunities into your future possibilities
      </p>
      <p>
        Jobvious is the easiest and quickest way to connect tech talent and
        working opportunities
      </p>
      <Button onClick={() => navigate("/login")} buttonName="Get Started" />
    </Layout>
  )
}
