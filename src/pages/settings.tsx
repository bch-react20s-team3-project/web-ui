import React from "react"

import Layout from "../components/Layout"
import Header from "../components/Header"
import { navigate } from "gatsby"

export default function Settings(): JSX.Element {
  const handleClick = () => alert("//TODO")

  const handleLogout = () => {
    localStorage.clear()
    navigate(`/get-started`, { state: { isAuth: false } })
  }

  //TODO add back (to profile) button
  return (
    <>
      <Header viewTitle="Settings" />
      <Layout menu="profile" className="pages-settings">
        <h3 onClick={handleClick}>Account</h3>
        <h3 onClick={handleClick}>My Location</h3>
        <h3 onClick={handleClick}>Notification Settings</h3>
        <h3 onClick={handleClick}>Terms &amp; Conditions</h3>
        <h3 onClick={handleClick}>Privacy</h3>
        <h3 onClick={handleClick}>FAQ</h3>
        <h3 onClick={handleLogout}>Logout</h3>
      </Layout>
    </>
  )
}
