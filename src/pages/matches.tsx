import React, { useState, useEffect } from "react"
import { CircularProgress } from "@material-ui/core"

import Layout from "../components/Layout"
import Match from "../components/Match"
import Header from "../components/Header"
import {
  getLikedAndMatchedProfiles,
  getUserRole,
} from "../services/dataQueryHandlers"
import { isTalentMatchType } from "../type-guards"

export default function Matches(): JSX.Element {
  const [matches, setMatches] = useState<TalentMatchType | PositionMatchType>()

  useEffect(() => {
    // Get data from backend server at run-time
    getUserRole().then(getLikedAndMatchedProfiles).then(setMatches)
  }, [])

  return (
    <>
      <Header viewTitle="Matches" />
      <Layout menu="matches" className="pages-matches">
        {/* <input type="text" className="matches-page-search" placeholder="Filter"></input> */}
        {/* TODO add search/filtering functionality (per position name for talent, per open positions for employer, per state (liked or matched)) */}
        {/* TODO add sort functionality (based on status timestamp) */}
        {matches === undefined ? (
          <CircularProgress
            color="inherit"
            style={{ marginTop: "2em", alignSelf: "center" }}
          />
        ) : matches?.liked.length || matches?.matched.length ? (
          <ul>
            {matches?.liked?.map(
              (profile: TalentProfileType | PositionProfileType) => (
                <Match
                  key={profile.id}
                  match={{
                    id: isTalentMatchType(matches)
                      ? profile.owner.id
                      : profile.owner.owners[0].id,
                    name: isTalentMatchType(matches)
                    ? profile.owner.shortDescription
                    : profile.owner.owners[0].name,
                    owner: profile.owner.username || profile.owner.companyName,
                    img: profile.img,
                    shortDescription: "",
                    status: "LIKED",
                    email: profile.owner.email,
                  }}
                />
              )
            )}
            {matches?.matched?.map(
              (profile: TalentProfileType | PositionProfileType) => (
                <Match
                  key={profile.id}
                  match={{
                    id: isTalentMatchType(matches)
                      ? profile.owner.id
                      : profile.owner.owners[0].id,
                      name: isTalentMatchType(matches)
                      ? profile.owner.shortDescription
                      : profile.owner.owners[0].name,
                    owner: profile.owner.username || profile.owner.companyName,
                    img: profile.img,
                    shortDescription: "",
                    status: "MATCH",
                    email: isTalentMatchType(matches)
                    ? profile.owner.email
                    : profile.owner.owners[0].email
                  }}
                />
              )
            )}
          </ul>
        ) : (
          <>
            <h1>There are no likes or matches yet.</h1>
            <h2>Please check the home view for more potential matches...</h2>
          </>
        )}
      </Layout>
    </>
  )
}
