import { gql, GraphQLClient } from "graphql-request"

import { isTalentMatchType } from "../type-guards"

const gqlCli = new GraphQLClient(`${process.env.GATSBY_STRAPI_URL}/graphql`)

//TODO @Vesa #JSDocs

export const updateEmployerProfile = async (
  data: EmployerProfileType
): Promise<void> =>
  new Promise((resolve, reject) => {
    // Assign auth header
    gqlCli.setHeader("Authorization", `Bearer ${localStorage.getItem("jwt")}`)

    // Grab the data props that can be updated directly with this input type
    const { img, companyName } = data

    // Make the mutation call
    gqlCli
      .request(
        gql`
          mutation updateEmployerProfile(
            $id: ID!
            $data: editEmployerProfileInput!
          ) {
            updateEmployerProfile(input: { where: { id: $id }, data: $data }) {
              employerProfile {
                id
              }
            }
          }
        `,
        { id: data.id, data: { img, companyName, draft: false } }
      )
      .then(resolve)
      .catch(reject)
  })

export const updateUser = async (data: UserType): Promise<void> =>
  new Promise((resolve, reject) => {
    // Assign auth header
    gqlCli.setHeader("Authorization", `Bearer ${localStorage.getItem("jwt")}`)

    // Grab the data props that can be updated directly with this input type
    const { username, email } = data

    // Make the mutation call
    gqlCli
      .request(
        gql`
          mutation updateUser($id: ID!, $data: editUserInput!) {
            updateUser(input: { where: { id: $id }, data: $data }) {
              user {
                id
              }
            }
          }
        `,
        { id: data.id, data: { username, email } }
      )
      .then(resolve)
      .catch(reject)
  })

export const updatePositionProfile = async (
  data: PositionProfileType
): Promise<void> =>
  new Promise((resolve, reject) => {
    // Assign auth header
    gqlCli.setHeader("Authorization", `Bearer ${localStorage.getItem("jwt")}`)

    // Grab the data props that can be updated directly with this input type
    const { description, jobType, location, name, img } = data
    // Skills are input only as an array of Skill.ids
    const requiredSkills = data.requiredSkills?.map(s => s.id)
    // Ensure this is input as a number
    const requiredExperience = Number(data.requiredExperience)

    // Make the mutation call
    gqlCli
      .request(
        gql`
          mutation updatePositionProfile(
            $id: ID!
            $data: editPositionProfileInput!
          ) {
            updatePositionProfile(input: { where: { id: $id }, data: $data }) {
              positionProfile {
                id
              }
            }
          }
        `,
        {
          id: data.id,
          data: {
            description,
            jobType,
            location,
            name,
            requiredExperience,
            requiredSkills,
            img,
          },
        }
      )
      .then(resolve)
      .catch(reject)
  })

export const updateTalentProfile = async (
  data: TalentProfileType
): Promise<void> =>
  new Promise((resolve, reject) => {
    // Assign auth header
    gqlCli.setHeader("Authorization", `Bearer ${localStorage.getItem("jwt")}`)

    // Grab the data props that can be updated directly with this input type
    const {
      description,
      jobTypeInterest,
      location,
      shortDescription,
      img,
    } = data
    // Skills are input only as an array of Skill.ids
    const skills = data.skills?.map(s => s.id)
    // Ensure this is input as a number
    const workExperience = Number(data.workExperience)

    // Make the mutation call
    gqlCli
      .request(
        gql`
          mutation updateTalentProfile(
            $id: ID!
            $data: editTalentProfileInput!
          ) {
            updateTalentProfile(input: { where: { id: $id }, data: $data }) {
              talentProfile {
                id
              }
            }
          }
        `,
        {
          id: data.id,
          data: {
            description,
            jobTypeInterest,
            workExperience,
            location,
            shortDescription,
            img,
            skills,
            draft: false,
          },
        }
      )
      .then(resolve)
      .catch(reject)
  })

export const createSkill = async (data: Partial<SkillType>): Promise<string> =>
  new Promise((resolve, reject) => {
    // Assign auth header
    gqlCli.setHeader("Authorization", `Bearer ${localStorage.getItem("jwt")}`)

    // Grab the data props that can be updated directly with this input type
    const { skillName } = data

    // Make the mutation call
    gqlCli
      .request(
        gql`
          mutation createSkill($data: SkillInput!) {
            createSkill(input: { data: $data }) {
              skill {
                id
              }
            }
          }
        `,
        {
          data: {
            skillName,
          },
        }
      )
      // Destructure response and resolve
      .then(({ createSkill: { skill: { id } } }) => resolve(id))
      .catch(reject)
  })

export const createPositionProfile = async (
  data: PositionProfileType
): Promise<void> =>
  new Promise((resolve, reject) => {
    // Assign auth header
    gqlCli.setHeader("Authorization", `Bearer ${localStorage.getItem("jwt")}`)

    // Grab the data props that can be updated directly with this input type
    const { description, jobType, location, name, img, owner } = data
    // Skills are input only as an array of Skill.ids
    const requiredSkills = data.requiredSkills?.map(s => s.id)
    // Ensure this is input as a number
    const requiredExperience = Number(data.requiredExperience)

    // Make the mutation call
    gqlCli
      .request(
        gql`
          mutation createPositionProfile($data: PositionProfileInput!) {
            createPositionProfile(input: { data: $data }) {
              positionProfile {
                id
                # FIXME add support for separate match list for each position
                # owner {
                #   owners {
                #     id
                #     matches {
                #       ... on ComponentMatchesDynamicTalentMatch {
                #         matches {
                #           id
                #         }
                #       }
                #     }
                #   }
                # }
              }
            }
          }
        `,
        {
          data: {
            description,
            jobType,
            location,
            name,
            requiredExperience,
            requiredSkills,
            img,
            owner,
          },
        }
      )
      // Destructure ids from the response and create also the corresponding match list for this new position
      .then(() =>
        // FIXME add support for separate match list for each position
        // {
        //   createPositionProfile: {
        //     positionProfile: {
        //       id: newPositionId,
        //       owner: {
        //         owners: [
        //           {
        //             id: userId,
        //             matches: [{ matches: currentMatches }],
        //           },
        //         ],
        //       },
        //     },
        //   },
        // }
        {
          resolve()

          // FIXME add support for separate match list for each position
          // addNewPositionToEmployerUserMatches(
          //   userId,
          //   currentMatches.map((m: { id: string }) => m.id),
          //   newPositionId
          // )
          //   .then(resolve)
          //   .catch(reject)
        }
      )
      .catch(reject)
  })

export const deletePositionProfile = async (id: string): Promise<void> =>
  new Promise((resolve, reject) => {
    // Assign auth header
    gqlCli.setHeader("Authorization", `Bearer ${localStorage.getItem("jwt")}`)

    // Make the mutation call
    gqlCli
      .request(
        gql`
          mutation deletePositionProfile($id: ID!) {
            deletePositionProfile(input: { where: { id: $id } }) {
              positionProfile {
                id
              }
            }
          }
        `,
        {
          id: id,
        }
      )
      .then(resolve)
      .catch(reject)
  })

export const updateMatches = async (
  data: PositionMatchType | TalentMatchType
): Promise<void> =>
  new Promise((resolve, reject) => {
    // Assign auth header
    gqlCli.setHeader("Authorization", `Bearer ${localStorage.getItem("jwt")}`)

    type PositionOrTalent = PositionProfileType | TalentProfileType

    // Grab the data props that can be updated directly with this input type
    const filtered = data.filtered?.map((p: PositionOrTalent) => p.id) || []
    const liked = data.liked?.map((p: PositionOrTalent) => p.id) || []
    const ignored = data.ignored?.map((p: PositionOrTalent) => p.id) || []
    const owner = data.owner.id

    // Make the mutation call
    isTalentMatchType(data)
      ? gqlCli
          .request(
            gql`
              mutation updateTalentMatch(
                $id: ID!
                $data: editTalentMatchInput!
              ) {
                updateTalentMatch(input: { where: { id: $id }, data: $data }) {
                  talentMatch {
                    id
                  }
                }
              }
            `,
            {
              id: data.id,
              data: {
                filtered,
                liked,
                ignored,
                owner,
              },
            }
          )
          .then(resolve)
          .catch(reject)
      : gqlCli
          .request(
            gql`
              mutation updatePositionMatch(
                $id: ID!
                $data: editPositionMatchInput!
              ) {
                updatePositionMatch(
                  input: { where: { id: $id }, data: $data }
                ) {
                  positionMatch {
                    id
                  }
                }
              }
            `,
            {
              id: data.id,
              data: {
                filtered,
                liked,
                ignored,
                owner,
              },
            }
          )
          .then(resolve)
          .catch(reject)
  })

export const register = async (data: {
  username: string
  email: string
  password: string
  desiredRole: "talent" | "employer"
}): Promise<{ jwt: string; id: string }> =>
  new Promise((resolve, reject) => {
    // Grab the data props
    const { username, email, password, desiredRole } = data

    // Make the mutation call
    gqlCli
      .request(
        gql`
          mutation register($data: UsersPermissionsRegisterInput!) {
            register(input: $data) {
              jwt
              user {
                id
              }
            }
          }
        `,
        {
          data: {
            username,
            email,
            password,
            desiredRole,
          },
        }
      )
      // Destructure response and resolve
      .then(
        ({
          register: {
            jwt,
            user: { id },
          },
        }) => {
          resolve({ jwt, id })
        }
      )
      // Destructure errors and reject
      .catch(
        ({
          response: {
            errors: [
              {
                extensions: {
                  exception: {
                    data: {
                      message: [
                        {
                          messages: [{ message }],
                        },
                      ],
                    },
                  },
                },
              },
            ],
          },
        }) => {
          reject(message)
        }
      )
  })

export const createTalentProfile = async (ownerId: string): Promise<void> =>
  new Promise((resolve, reject) => {
    // Assign auth header
    gqlCli.setHeader("Authorization", `Bearer ${localStorage.getItem("jwt")}`)

    // Assign create talent profile mutation query and variables
    const profileQuery = gql`
      mutation createTalentProfile($data: TalentProfileInput!) {
        createTalentProfile(input: { data: $data }) {
          talentProfile {
            id
          }
        }
      }
    `
    const profileVars = {
      data: {
        owner: ownerId,
        shortDescription: "Short description of your specialization...",
        location: "Your location...",
        workExperience: 0,
        description: "Full description of your talent profile...",
        draft: true,
        jobTypeInterest: "fulltime",
      },
    }

    // Assign update user mutation query and vars
    const userQuery = gql`
      mutation updateUser($id: ID!, $data: editUserInput!) {
        updateUser(input: { where: { id: $id }, data: $data }) {
          user {
            id
          }
        }
      }
    `
    const userVars = {
      profile: [
        {
          __typename: "ComponentProfileDynamicTalentProfile", // User.profile is a dynamic zone property so assign type
          profile: "", // Placeholder for id of the to-be-created profile
        },
      ],
    }

    // Make the mutation calls to create profile and update user.profile reference to it
    gqlCli
      .request(profileQuery, profileVars)
      // Destructure response
      .then(
        ({
          createTalentProfile: {
            talentProfile: { id },
          },
        }) => {
          // Update user mutation variables with the newly created talent profile id
          userVars.profile[0].profile = id

          // Make the user mutation
          gqlCli
            .request(userQuery, {
              id: ownerId,
              data: userVars,
            })
            .then(resolve)
            .catch(reject)
        }
      )
      .catch(reject)
  })

export const createEmployerProfile = async (ownerId: string): Promise<void> =>
  new Promise((resolve, reject) => {
    // Assign auth header
    gqlCli.setHeader("Authorization", `Bearer ${localStorage.getItem("jwt")}`)

    // Assign create talent profile mutation query and variables
    const profileQuery = gql`
      mutation createEmployerProfile($data: EmployerProfileInput!) {
        createEmployerProfile(input: { data: $data }) {
          employerProfile {
            id
          }
        }
      }
    `
    const profileVars = {
      data: {
        owners: [ownerId],
        companyName: "Your company...",
        draft: true,
      },
    }

    // Assign update user mutation query and vars
    const userQuery = gql`
      mutation updateUser($id: ID!, $data: editUserInput!) {
        updateUser(input: { where: { id: $id }, data: $data }) {
          user {
            id
          }
        }
      }
    `
    const userVars = {
      profile: [
        {
          __typename: "ComponentProfileDynamicEmployerProfile", // User.profile is a dynamic zone property so assign type
          profile: "", // Placeholder for id of the to-be-created profile
        },
      ],
    }

    // Make the mutation calls to create profile and update user.profile reference to it
    gqlCli
      .request(profileQuery, profileVars)
      // Destructure response
      .then(
        ({
          createEmployerProfile: {
            employerProfile: { id },
          },
        }) => {
          // Update user mutation variables with the newly created talent profile id
          userVars.profile[0].profile = id

          // Make the user mutation
          gqlCli
            .request(userQuery, { id: ownerId, data: userVars })
            .then(resolve)
            .catch(reject)
        }
      )
      .catch(reject)
  })

export const createPositionMatches = async (ownerId: string): Promise<void> =>
  new Promise((resolve, reject) => {
    // Assign auth header
    gqlCli.setHeader("Authorization", `Bearer ${localStorage.getItem("jwt")}`)

    // Assign create position matches mutation query
    const matchesQuery = gql`
      mutation createPositionMatch($ownerId: ID!) {
        createPositionMatch(input: { data: { owner: $ownerId } }) {
          positionMatch {
            id
          }
        }
      }
    `
    const matchesVars = {
      ownerId,
    }

    // Assign update user mutation query and vars
    const userQuery = gql`
      mutation updateUser($id: ID!, $data: editUserInput!) {
        updateUser(input: { where: { id: $id }, data: $data }) {
          user {
            id
          }
        }
      }
    `
    const userVars = {
      matches: [
        {
          __typename: "ComponentMatchesDynamicPositionMatch", // User.matches is a dynamic zone property so assign type
          matches: "", // Placeholder for the id of the to-be-created matches list
        },
      ],
    }

    // Make the mutation calls to create matches list and update user.matches reference to it
    gqlCli
      .request(matchesQuery, matchesVars)
      // Destructure response
      .then(
        ({
          createPositionMatch: {
            positionMatch: { id },
          },
        }) => {
          // Update user mutation variables with the newly created matches list id
          userVars.matches[0].matches = id

          // Make the user mutation
          gqlCli
            .request(userQuery, { id: ownerId, data: userVars })
            .then(resolve)
            .catch(reject)
        }
      )
      .catch(reject)
  })

export const createTalentMatches = async (ownerId: string): Promise<void> =>
  new Promise((resolve, reject) => {
    // Assign auth header
    gqlCli.setHeader("Authorization", `Bearer ${localStorage.getItem("jwt")}`)

    // Assign create talent matches mutation query
    const matchesQuery = gql`
      mutation createTalentMatch($ownerId: ID!) {
        createTalentMatch(input: { data: { owner: $ownerId } }) {
          talentMatch {
            id
          }
        }
      }
    `
    const matchesVars = {
      ownerId,
    }

    // Assign update user mutation query and vars
    const userQuery = gql`
      mutation updateUser($id: ID!, $data: editUserInput!) {
        updateUser(input: { where: { id: $id }, data: $data }) {
          user {
            id
          }
        }
      }
    `
    const userVars = {
      matches: [
        {
          __typename: "ComponentMatchesDynamicTalentMatch", // User.matches is a dynamic zone property so assign type
          matches: [""], // Placeholder for the id of the to-be-created matches list
        },
      ],
    }

    // Make the mutation calls to create matches list and update user.matches reference to it
    gqlCli
      .request(matchesQuery, matchesVars)
      // Destructure response
      .then(
        ({
          createTalentMatch: {
            talentMatch: { id },
          },
        }) => {
          // Update user mutation variables with the newly created matches list id
          userVars.matches[0].matches = [id]

          // Make the user mutation
          gqlCli
            .request(userQuery, { id: ownerId, data: userVars })
            .then(resolve)
            .catch(reject)
        }
      )
      .catch(reject)
  })

export const login = async (data: {
  email: string
  password: string
}): Promise<{ jwt: string; id: string }> =>
  new Promise((resolve, reject) => {
    // Grab the data props
    const { email, password } = data

    // Make the mutation call
    gqlCli
      .request(
        gql`
          mutation login($data: UsersPermissionsLoginInput!) {
            login(input: $data) {
              jwt
              user {
                id
              }
            }
          }
        `,
        {
          data: {
            identifier: email,
            password,
          },
        }
      )
      // Destructure response and resolve
      .then(
        ({
          login: {
            jwt,
            user: { id },
          },
        }) => {
          resolve({ jwt, id })
        }
      )
      // Destructure errors and reject
      .catch(
        ({
          response: {
            errors: [
              {
                extensions: {
                  exception: {
                    data: {
                      message: [
                        {
                          messages: [{ message }],
                        },
                      ],
                    },
                  },
                },
              },
            ],
          },
        }) => {
          reject(message)
        }
      )
  })

//FIXME add support for separate match lists for each position
export const addNewPositionToEmployerUserMatches = async (
  userId: string,
  currentMatches: [string],
  newPositionId: string
): Promise<void> =>
  new Promise((resolve, reject) => {
    // Assign auth header
    gqlCli.setHeader("Authorization", `Bearer ${localStorage.getItem("jwt")}`)

    // Assign create talent matches mutation query and variables
    const matchesQuery = gql`
      mutation createTalentMatch($newPositionId: ID) {
        createTalentMatch(input: { data: { forPosition: $newPositionId } }) {
          talentMatch {
            id
          }
        }
      }
    `
    const matchesVars = {
      newPositionId: newPositionId,
    }

    // Assign the user mutation query and variables
    const userQuery = gql`
      mutation updateUser($id: ID!, $data: editUserInput!) {
        updateUser(input: { where: { id: $id }, data: $data }) {
          user {
            id
          }
        }
      }
    `
    const userVars = {
      matches: [
        {
          __typename: "ComponentMatchesDynamicTalentMatch", // User.matches is a dynamic zone property so assign type
          matches: currentMatches,
        },
      ],
    }

    // Make the mutation calls to create the new matches list and update user.matches reference to it
    gqlCli
      .request(matchesQuery, matchesVars)
      // Destructure response
      .then(
        ({
          createTalentMatch: {
            talentMatch: { id },
          },
        }) => {
          // Update user mutation variables with the newly created matches list id
          userVars.matches[0].matches.push(id)

          // Make the user mutation
          gqlCli
            .request(userQuery, { id: userId, data: userVars })
            .then(resolve)
            .catch(reject)
        }
      )
      .catch(reject)
  })
