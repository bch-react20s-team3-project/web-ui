import React from "react"

const updateState = <T,>(state: T, updates: Partial<T>) =>
  Object.assign({}, state, updates)

export const updateImg = <
  T extends EmployerProfileType | PositionProfileType | TalentProfileType
>(
  data: T,
  { target: { files } }: React.BaseSyntheticEvent,
  callback: React.Dispatch<React.SetStateAction<T>>
): void => {
  const fr = new FileReader()
  fr.readAsDataURL(files[0])
  fr.onloadend = () =>
    callback(updateState(data, { img: String(fr.result) } as T))
}

export const updateOwners = <T extends EmployerProfileType>(
  data: T,
  { target: { name, value } }: React.BaseSyntheticEvent,
  callback: React.Dispatch<React.SetStateAction<T>>
): void => {
  const owners = data.owners
  owners[0][name] = value
  callback(updateState(data, { owners: owners } as T))
}

export const updateOwner = <T extends PositionProfileType | TalentProfileType>(
  data: T,
  { target: { name, value } }: React.BaseSyntheticEvent,
  callback: React.Dispatch<React.SetStateAction<T>>
): void => {
  const owner = data.owner
  owner[name] = value
  callback(updateState(data, { owner: owner } as T))
}

export const updateText = <
  T extends EmployerProfileType | PositionProfileType | TalentProfileType
>(
  data: T,
  { target: { name, value } }: React.BaseSyntheticEvent,
  callback: React.Dispatch<React.SetStateAction<T>>
): void => {
  callback(updateState(data, { [name]: value } as T))
}

export const addSkill = <T extends PositionProfileType | TalentProfileType>(
  data: T,
  { target: { name, value } }: React.BaseSyntheticEvent,
  callback: React.Dispatch<React.SetStateAction<T>>
): void => {
  const skills = data[name] //FIXME typing
  // First skill to be added
  if (!skills)
    callback(
      updateState(data, {
        [name]: [value],
      } as T)
    )
  // Add skill to existing list
  else if (!skills.map((s: SkillType) => s.skillName).includes(value))
    callback(
      updateState(data, {
        [name]: [...skills, value],
      } as T)
    )
}

export const removeSkill = <T extends PositionProfileType | TalentProfileType>(
  data: T,
  { id, name }: { id: string; name: string },
  callback: React.Dispatch<React.SetStateAction<T>>
): void => {
  const skills = data[name] //FIXME typing
  callback(
    updateState(data, {
      [name]: skills.filter((s: SkillType) => s.id !== id),
    } as T)
  )
}
