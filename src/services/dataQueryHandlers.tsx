import { gql, GraphQLClient } from "graphql-request"

const gqlClient = new GraphQLClient(`${process.env.GATSBY_STRAPI_URL}/graphql`)

//TODO @Vesa #JSDocs

export const getUserProfileId = async (): Promise<string> =>
  new Promise((resolve, reject) => {
    // Assign auth header
    gqlClient.setHeader(
      "Authorization",
      `Bearer ${localStorage.getItem("jwt")}`
    )

    // Try to query for talent profile first
    gqlClient
      .request(
        gql`
          {
            me {
              user {
                profile {
                  ... on ComponentProfileDynamicTalentProfile {
                    profile {
                      id
                    }
                  }
                }
              }
            }
          }
        `
      )
      // Destructure response and request for employer profile if talent profile was not found
      .then(
        ({
          me: {
            user: { profile },
          },
        }) => {
          if (profile[0].profile) resolve(profile[0].profile.id)
          else {
            gqlClient
              .request(
                gql`
                  {
                    me {
                      user {
                        profile {
                          ... on ComponentProfileDynamicEmployerProfile {
                            profile {
                              id
                            }
                          }
                        }
                      }
                    }
                  }
                `
              )
              // Destructure response and resolve
              .then(
                ({
                  me: {
                    user: { profile },
                  },
                }) => {
                  if (profile[0].profile) resolve(profile[0].profile.id)
                }
              )
              .catch(reject)
          }
        }
      )
      .catch(reject)
  })

export const getUserRole = async (): Promise<string> =>
  new Promise((resolve, reject) => {
    // Assign auth header
    gqlClient.setHeader(
      "Authorization",
      `Bearer ${localStorage.getItem("jwt")}`
    )

    gqlClient
      .request(
        gql`
          {
            me {
              role {
                name
              }
            }
          }
        `
      )
      // Destructure response and resolve
      .then(({ me: { role: { name } } }) => resolve(name))
      .catch(reject)
  })

export const getFilteredTalentProfiles = async (): Promise<TalentMatchType> =>
  new Promise((resolve, reject) => {
    // Assign auth header
    gqlClient.setHeader(
      "Authorization",
      `Bearer ${localStorage.getItem("jwt")}`
    )

    gqlClient
      .request(
        gql`
          {
            me {
              user {
                matches {
                  ... on ComponentMatchesDynamicTalentMatch {
                    matches {
                      id
                      forPosition {
                        id
                      }
                      filtered {
                        id
                      }
                      liked {
                        id
                      }
                      ignored {
                        id
                      }
                      owner {
                        id
                      }
                    }
                  }
                }
              }
            }
          }
        `
      )
      // Destructure response and resolve
      .then(
        ({
          me: {
            user: { matches },
          },
        }) => {
          resolve(matches[0].matches[0]) //FIXME support for position specific match lists
        }
      )
      .catch(reject)
  })

export const getFilteredPositionProfiles = async (): Promise<PositionMatchType> =>
  new Promise((resolve, reject) => {
    // Assign auth header
    gqlClient.setHeader(
      "Authorization",
      `Bearer ${localStorage.getItem("jwt")}`
    )

    gqlClient
      .request(
        gql`
          {
            me {
              user {
                matches {
                  ... on ComponentMatchesDynamicPositionMatch {
                    matches {
                      id
                      filtered {
                        id
                      }
                      liked {
                        id
                      }
                      ignored {
                        id
                      }
                      owner {
                        id
                      }
                    }
                  }
                }
              }
            }
          }
        `
      )
      // Destructure response and resolve
      .then(
        ({
          me: {
            user: { matches },
          },
        }) => {
          resolve(matches[0].matches)
        }
      )
      .catch(reject)
  })

export const getLikedAndMatchedProfiles = async (
  role: string
): Promise<TalentMatchType | PositionMatchType> =>
  new Promise((resolve, reject) => {
    // Assign auth header
    gqlClient.setHeader(
      "Authorization",
      `Bearer ${localStorage.getItem("jwt")}`
    )

    // Assign GQL query
    let query = ""
    if (role === "talent")
      query = gql`
        {
          me {
            user {
              matches {
                ... on ComponentMatchesDynamicPositionMatch {
                  matches {
                    liked {
                      owner {
                        id
                        owners {
                          id
                          email
                        }
                        companyName
                      }
                      name
                      img
                    }
                    matched {
                      owner {
                        id
                        owners {
                          id
                          email
                        }
                        companyName
                      }
                      name
                      img
                    }
                  }
                }
              }
            }
          }
        }
      `
    if (role === "employer")
      query = gql`
        {
          me {
            user {
              matches {
                ... on ComponentMatchesDynamicTalentMatch {
                  matches {
                    forPosition {
                      id
                    }
                    liked {
                      id
                      owner {
                        id
                        username
                        email
                      }
                      shortDescription
                      img
                    }
                    matched {
                      id
                      owner {
                        id
                        username
                        email
                      }
                      shortDescription
                      img
                    }
                  }
                }
              }
            }
          }
        }
      `

    gqlClient
      .request(query)
      // Destructure response and resolve
      .then(
        ({
          me: {
            user: {
              matches: [{ matches }],
            },
          },
        }) => {
          if (role === "talent")
            resolve(matches ? matches : { matched: [], liked: [] })
          if (role === "employer")
            resolve(matches ? matches[0] : { matched: [], liked: [] })
        }
      )
      .catch(reject)
  })

export const getSkills = async (): Promise<SkillType[]> =>
  new Promise((resolve, reject) => {
    // Assign auth header
    gqlClient.setHeader(
      "Authorization",
      `Bearer ${localStorage.getItem("jwt")}`
    )

    gqlClient
      .request(
        gql`
          {
            skills {
              id
              skillName
            }
          }
        `
      )
      // Destructure response and resolve
      .then(({ skills }) => resolve(skills))
      .catch(reject)
  })
