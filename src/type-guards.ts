export const isPositionProfile = (
  item: TalentProfileType | PositionProfileType
): item is PositionProfileType =>
  (item as PositionProfileType).name !== undefined

export const isEmployerUser = (
  item: TalentProfileType | EmployerProfileType
): item is EmployerProfileType =>
  (item as EmployerProfileType).owners !== undefined

export const isTalentMatchType = (
  item: PositionMatchType | TalentMatchType
): item is TalentMatchType =>
  (item as TalentMatchType).forPosition !== undefined
