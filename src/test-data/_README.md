# Test data JSONs
This folder is for dummy test data that would in the final app be queried from the backend server. JSON files are named per API - e.g. `positions.json` maps to backend api `/positions`, or `user-matches.json` maps to `/user/matches`.
