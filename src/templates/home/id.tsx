import React, { useState, useEffect } from "react"
import { navigate } from "gatsby"
import ThumbDownIcon from "@material-ui/icons/ThumbDown"
import ThumbUpIcon from "@material-ui/icons/ThumbUp"

import Layout from "../../components/Layout"
import TalentProfile from "../../components/TalentProfile"
import PositionProfile from "../../components/PositionProfile"
import { isPositionProfile } from "../../type-guards"
import { updateMatches } from "../../services/dataMutationHandlers"

interface HomeTemplate {
  pageContext: {
    id: string
    data: TalentProfileType | PositionProfileType
  }
  location?: {
    state: {
      browsing: boolean
      matches: PositionMatchType | TalentMatchType
    }
  }
}

/**
 * @param pageContext Page id and data to use to build the page.
 * @param location Run-time state to apply on to the page.
 * @returns JSX.Element
 */
export default function HomeTemplate({
  pageContext,
  location,
}: HomeTemplate): JSX.Element {
  const { id, data } = pageContext // build-time data from gatsby-node.js

  // run-time state for browser
  const [browsing, setBrowsing] = useState(false) // prev/next browsing should not be shown if the user has not arrived thru the main home-page
  const [matches, setMatches] = useState<TalentMatchType | PositionMatchType>()
  const [next, setNext] = useState("")

  useEffect(() => {
    setBrowsing(location?.state?.browsing || false)
    setMatches(location?.state?.matches)

    const filteredIds = location?.state?.matches.filtered.map(
      (m: { id: string }) => m.id
    )

    const currentIndex = filteredIds?.findIndex(matchId => matchId === id)

    // Deduce ids of prev and next profile page (if match list is available)
    if (filteredIds && currentIndex !== undefined)
      setNext(
        currentIndex === filteredIds.length - 1
          ? filteredIds[0]
          : filteredIds[currentIndex + 1]
      )
  }, [])

  const updateState = <T,>(state: T, updates: Partial<T>) =>
    Object.assign({}, state, updates)

  const handleLike = (likeOrIgnore: "liked" | "ignored") => {
    if (matches) {
      // Create updated matches list (liked/ignored removed from filtered matches and added to the liked/ignored list)
      const updatedMatches = updateState(matches, {
        //FIXME: typing
        filtered: matches.filtered.filter((p: { id: string }) => p.id !== id),
        [likeOrIgnore]: [data].concat(matches[likeOrIgnore]),
      } as typeof matches)

      // Make backend mutation call
      updateMatches(updatedMatches)

      // Move to next one (if there are still filtered matches left) or back to home root
      updatedMatches.filtered.length
        ? navigate(`/home/${next}`, {
            state: { browsing: true, matches: updatedMatches },
          })
        : navigate(`/home`)
    }
  }

  return (
    <Layout menu="home">
      <div className="pages-home">
        {isPositionProfile(data) ? (
          <PositionProfile data={data} />
        ) : (
          <TalentProfile data={data} />
        )}
        {/* Show buttons only if browsing (e.g. vs. arriving directly thru a bookmark) */}
      </div>
      <>
        {browsing && (
          <div className="pages-home-like-buttons">
            <ThumbDownIcon onClick={() => handleLike("ignored")} />
            <ThumbUpIcon onClick={() => handleLike("liked")} />
          </div>
        )}
      </>
    </Layout>
  )
}
