import React, { useState, useEffect } from "react"
import { navigate } from "gatsby"
import Modal from "@material-ui/core/Modal"

import Layout from "../../components/Layout"
import EmployerProfile from "../../components/EmployerProfile"
import TalentProfile from "../../components/TalentProfile"
import { isEmployerUser } from "../../type-guards"
import { getUserProfileId } from "../../services/dataQueryHandlers"

interface ProfileTemplate {
  pageContext: {
    id: string
    data: TalentProfileType | EmployerProfileType
  }
}

/**
 * @param pageContext Page id and data to use to build the page.
 * @returns JSX.Element
 */
export default function ProfileTemplate({
  pageContext,
}: ProfileTemplate): JSX.Element {
  const { data } = pageContext // build-time data from gatsby-node.js

  const [showModal, setShowModal] = useState(data.draft)

  useEffect(() => {
    getUserProfileId()
      .then(id => {
        id !== pageContext.id && navigate(`/profile/${id}`)
      })
      .catch(console.log)
  }, [])

  const handleClose = () => {
    setShowModal(false)
  }

  return (
    <>
      <Modal open={showModal} onClose={handleClose}>
        <h1 style={{ padding: "1em", textAlign: "center" }}>
          Please complete your profile to start receiving likes and matches!
        </h1>
      </Modal>
      <Layout menu="profile" className="pages-profile">
        {isEmployerUser(data) ? (
          <>
            <EmployerProfile data={data} editable={true} />
            <h3
              onClick={() =>
                navigate(`/position`, { state: { owner: data.id } })
              }
              style={{ display: "flex", justifyContent: "space-between" }}
            >
              <span>Create a job position</span>
              <span>{">"}</span>
            </h3>
          </>
        ) : (
          <TalentProfile data={data} editable={true} />
        )}

        <h3
          onClick={() => navigate(`/settings`)}
          style={{ display: "flex", justifyContent: "space-between" }}
        >
          <span>Settings</span>
          <span>{">"}</span>
        </h3>
      </Layout>
    </>
  )
}
