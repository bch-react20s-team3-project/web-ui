import React from "react"

import Layout from "../../components/Layout"
import PositionProfile from "../../components/PositionProfile"

interface PositionPage {
  pageContext: {
    id: string
    data: PositionProfileType
  }
}

/**
 * @param pageContext Page id and data to use to build the page.
 * @returns JSX.Element
 */
export default function PositionPage({
  pageContext,
}: PositionPage): JSX.Element {
  const { data } = pageContext

  return (
    <Layout menu="profile" className="pages-profile">
      <PositionProfile data={data} editable={true} />
    </Layout>
  )
}
