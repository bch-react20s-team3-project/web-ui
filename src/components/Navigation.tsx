import React from "react"
import { Link } from "gatsby"
import HomeIcon from "@material-ui/icons/Home"
import StarIcon from "@material-ui/icons/Star"
import AccountBoxIcon from "@material-ui/icons/AccountBox"

interface Navigation {
  menu?: string
}

/**
 * @param {string} menu Enum for which menu icon to highlight ("home" | "matches" | "profile")
 * @returns JSX.Element
 */
export default function Navigation({ menu }: Navigation): JSX.Element {
  return (
    <nav className="components-navigation">
      <ul>
        <li>
          <Link to="/home">
            {menu === "home" ? (
              <HomeIcon fontSize="large" style={{ color: "EF7066" }} />
            ) : (
              <HomeIcon fontSize="large" />
            )}
          </Link>
        </li>
        <li>
          <Link to="/matches">
            {menu === "matches" ? (
              <StarIcon fontSize="large" style={{ color: "EF7066" }} />
            ) : (
              <StarIcon fontSize="large" />
            )}
          </Link>
        </li>
        <li>
          <Link to="/profile">
            {menu === "profile" ? (
              <AccountBoxIcon fontSize="large" style={{ color: "EF7066" }} />
            ) : (
              <AccountBoxIcon fontSize="large" />
            )}
          </Link>
        </li>
      </ul>
    </nav>
  )
}
