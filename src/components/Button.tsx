import React from "react"

interface Button {
  onClick: React.MouseEventHandler<HTMLButtonElement>
  className?: string
  buttonName: string
}

/**
 * @param {React.MouseEventHandler<HTMLButtonElement>} onClick Click handler fn to use.
 * @param {string} className Class names to apply to the button.
 * @param {string} buttonName Button label to use.
 * @returns JSX.Element
 */
export default function Button({
  onClick,
  className,
  buttonName,
}: Button): JSX.Element {
  return (
    <button onClick={onClick} className={`button ${className}`}>
      {buttonName}
    </button>
  )
}
