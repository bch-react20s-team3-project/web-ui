import React from "react"

interface Header {
  viewTitle: string
}

/**
 * @param {string} viewTitle Title to show for the page header.
 * @returns JSX.Element
 */
export default function Header({ viewTitle }: Header): JSX.Element {
  return (
    <header>
      <h2>{viewTitle}</h2>
    </header>
  )
}
