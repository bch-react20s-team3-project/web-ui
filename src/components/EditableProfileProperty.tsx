import React from "react"
import CheckCircleIcon from "@material-ui/icons/CheckCircle"
//import CancelIcon from "@material-ui/icons/Cancel"
import EditIcon from "@material-ui/icons/Edit"

interface EditableProfileProperty {
  baseElement: JSX.Element | string | number
  editElement: JSX.Element
  state: {
    editState: boolean
    showBaseWhileEditing?: boolean
    showButtons?: boolean
  }
  handlers: {
    handleEdit: () => void
    handleSave: () => void
    //handleCancel: () => void
  }
}

/**
 * @param {JSX.Element | string | number} baseElement Element to use when not editing (editState = false).
 * @param {JSX.Element} editElement Element to use when editing (editState = true). Note that you must set the needed change handler to the element's onChange listener.
 * @param {{ showBaseWhileEditing, editState, showButtons }} state Set showBaseWhileEditing to true if the baseElement should be shown together with the editElement when editState = true. Set editState to true/false for the current desired state (edit/base respectively) of the component. Set showButtons Flag to enable/disable edit/save/cancel buttons visibility. Defaults to false.
 * @param {{ handleEdit, handleSave }} handlers Handler for toggling edit state on (handleEdit) and off (handleSave). Actual saving of the input value is expected to be handled by the editElement onChange etc. events.
 * @returns JSX.Element
 */
export default function EditableProfileProperty({
  baseElement,
  editElement,
  state: { editState, showBaseWhileEditing, showButtons = false },
  handlers: { handleEdit, handleSave },
}: EditableProfileProperty): JSX.Element {
  const childElements = (
    <>
      {editState ? (
        <>
          {showBaseWhileEditing && baseElement}
          {/* TODO {showButtons && <CancelIcon className="button" onClick={handleCancel} />} */}
          {editElement}
          {showButtons && (
            <CheckCircleIcon className="button" onClick={handleSave} />
          )}
        </>
      ) : (
        <>
          {baseElement}
          {showButtons && <EditIcon className="button" onClick={handleEdit} />}
        </>
      )}
    </>
  )

  return <div className="editable-profile-property">{childElements}</div>
}
