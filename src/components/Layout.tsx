/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.com/docs/use-static-query/
 */
import React from "react"
import Navigation from "./Navigation"
import "../scss/main.scss"

interface Layout {
  nav?: boolean
  menu?: "home" | "matches" | "profile"
  children?: JSX.Element | JSX.Element[] | boolean
  className?: string
}

/**
 * @param {boolean} nav Show navigation footer, defaults to true
 * @param {"home" | "matches" | "profile"} menu Which navigation icon to highlight
 * @param {JSX.Element | JSX.Element[] | boolean} children Child element(s)
 * @param {string} className Classes to apply to the Layout container
 * @returns JSX.Element
 */
export default function Layout({
  nav = true,
  menu,
  children,
  className,
}: Layout): JSX.Element {
  return (
    <div className={className}>
      <main>{children}</main>
      {nav && (
        <footer>
          <Navigation menu={menu} />
        </footer>
      )}
    </div>
  )
}
