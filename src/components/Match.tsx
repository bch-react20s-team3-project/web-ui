import React from "react"
import { navigate } from "gatsby"


import Avatar from "../components/Avatar"

interface Match {
  match: {
    id: string
    name: string
    owner: string
    img?: string // base64
    shortDescription?: string
    status: "LIKED" | "MATCH"
    email: string
  }
}

/**
 * @param {string} id Match id
 * @param {string} name Match name, talent user short desc. or position name
 * @param {string} owner Match owner (talent user name or position owner company name)
 * @param {string} img Base64 image data for the match avatar
 * @param {string} shortDescription Short additional description
 * @param {string} status Match status (liked/match)
 * @param {string} email Match owner email
 * @returns
 */
export default function Match({
  match: { id, name, owner, img, shortDescription, status, email },
}: Match): JSX.Element {
  const handleClick = () =>
    navigate("/chat", { state: { id, name: owner, email, img } })

  return (
    <li key={id} className="component-match">
      <Avatar src={img || ""} size="small" />
      <div>
        <div className="matches-name">{name}</div>
        <div className="matches-owner">{owner}</div>
        <div className="matches-description">{shortDescription}</div>
      </div>
      <div className="matches-actions">
        <div className="matches-status">{status}</div>
        {status === ("MATCH") && (
          <button className="matches-button" onClick={handleClick}>
            MESSAGE
          </button>
        )}
      </div>
    </li>
  )
}
