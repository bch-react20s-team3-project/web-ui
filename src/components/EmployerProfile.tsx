import React, { useState } from "react"

import Avatar from "./Avatar"
import EditableProfileProperty from "./EditableProfileProperty"
import PositionList from "./PositionList"
import * as handlers from "../services/componentStateUpdateHandlers"
import {
  updateEmployerProfile,
  updateUser,
} from "../services/dataMutationHandlers"

interface EmployerProfile {
  data: EmployerProfileType
  editable?: boolean
}

/**
 * @param { EmployerProfileComponentType } data Employer profile data to use.
 * @param { boolean } editable Flag whether to show edit buttons.
 * @returns JSX.Element
 */
export default function EmployerProfile({
  data: propData,
  editable,
}: EmployerProfile): JSX.Element {
  const [data, setData] = useState(propData)
  const [editField, setEditField] = useState("none")

  const profilePropertyState = (
    editState = false,
    showBaseWhileEditing = false,
    showButtons = editable
  ) => {
    return {
      editState: editState,
      showBaseWhileEditing: showBaseWhileEditing,
      showButtons: showButtons,
    }
  }

  const profilePropertyHandlers = (editField: string) => {
    return {
      handleEdit: () => setEditField(editField),
      handleSave: () => setEditField("none"),
    }
  }

  const onImgChange = (e: React.BaseSyntheticEvent) =>
    handlers.updateImg(data, e, setData)

  const onOwnersChange = (e: React.BaseSyntheticEvent) =>
    handlers.updateOwners(data, e, setData)

  const onTextChange = (e: React.BaseSyntheticEvent) =>
    handlers.updateText(data, e, setData)

  // Post data mutations to Strapi
  const handlePublish = () => {
    //TODO conditions to update only if there are actual changes
    updateEmployerProfile(data).catch(console.log)
    updateUser(data.owners[0]).catch(console.log)
  }

  return (
    <>
      <section>
        {/* user img */}
        <EditableProfileProperty
          baseElement={<Avatar size="large" src={String(data.img)} />}
          editElement={<input name="img" type="file" onChange={onImgChange} />}
          state={profilePropertyState(editField === "img", true)}
          handlers={profilePropertyHandlers("img")}
        />

        {/* user name */}
        <EditableProfileProperty
          baseElement={data.owners[0].username || ""}
          editElement={
            <input
              name="username"
              type="text"
              value={data.owners[0].username}
              onChange={onOwnersChange}
            />
          }
          state={profilePropertyState(editField === "username")}
          handlers={profilePropertyHandlers("username")}
        />

        {/* user email */}
        {editable && (
          <EditableProfileProperty
            baseElement={data.owners[0].email || ""}
            editElement={
              <input
                name="email"
                type="text"
                value={data.owners[0].email}
                onChange={onOwnersChange}
              />
            }
            state={profilePropertyState(editField === "email")}
            handlers={profilePropertyHandlers("email")}
          />
        )}
      </section>

      <section>
        {/* user companyName */}
        <EditableProfileProperty
          baseElement={data.companyName || ""}
          editElement={
            <input
              name="companyName"
              type="text"
              value={data.companyName}
              onChange={onTextChange}
            />
          }
          state={profilePropertyState(editField === "companyName")}
          handlers={profilePropertyHandlers("companyName")}
        />
      </section>

      {/* user positions */}
      <section>
        <b>Positions</b>
        <PositionList positions={data.positions} showButtons={editable} />
      </section>

      {editable && (
        <h3
          onClick={handlePublish}
          style={{ display: "flex", justifyContent: "space-between" }}
        >
          <span>Save changes</span>
          <span>{">"}</span>
        </h3>
      )}
    </>
  )
}
