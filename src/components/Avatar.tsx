import React from "react"
import { makeStyles, createStyles } from "@material-ui/core/styles"
import { Avatar as MuiAvatar } from "@material-ui/core"

const useStyles = makeStyles(() =>
  createStyles({
    small: {
      width: "45px",
      height: "45px",
    },
    large: {
      width: "80px",
      height: "80px",
    },
  })
)

interface Avatar {
  src: string
  size: "small" | "large"
}

/**
 * @param {string} src Base64 encoded string for the source image to use.
 * @returns JSX.Element
 */
export default function Avatar({ src, size }: Avatar): JSX.Element {
  const classes = useStyles()
  return <MuiAvatar src={String(src)} className={classes[size]} />
}
