import React from "react"
import { Link } from "gatsby"
import EditIcon from "@material-ui/icons/Edit"

interface EditablePositionList {
  positions?: PositionProfileType[]
  showButtons?: boolean
}

/**
 * @param {PositionProfileType[]} positions Data to use.
 * @param {boolean} showButtons Flag whether to show the edit controls. Defaults to false.
 * @returns JSX.Element
 */
export default function EditablePositionList({
  positions = [],
  showButtons = false,
}: EditablePositionList): JSX.Element {
  return (
    <ul>
      {positions.length ? (
        positions.map(p => (
          <li key={p.id}>
            <div className="editable-profile-property">
              <b>{p.name}</b>
              <p>
                {p.location}, {p.jobType}
              </p>
              {showButtons && (
                <div className="button">
                  <Link to={`/position/${p.id}`} state={{ data: p }}>
                    <EditIcon />
                  </Link>
                </div>
              )}
            </div>
          </li>
        ))
      ) : (
        <li>You have not added any positions yet!</li>
      )}
    </ul>
  )
}
