import React, { useState, useEffect } from "react"
import DeleteIcon from "@material-ui/icons/Delete"
import TextField from "@material-ui/core/TextField"
import Autocomplete, {
  createFilterOptions,
} from "@material-ui/lab/Autocomplete"
import { FilterOptionsState } from "@material-ui/lab"

import { getSkills } from "../services/dataQueryHandlers"
import { createSkill } from "../services/dataMutationHandlers"

const filter = createFilterOptions<SkillType & { inputValue?: string }>()

interface EditableSkillList {
  skills: SkillType[]
  showButtons?: boolean
  handlers: {
    handleAdd?: (e: React.BaseSyntheticEvent) => void
    handleDelete?: (id: string, name: string) => void
  }
  name: string
}

/**
 * @param {SkillType[]} skills Data to use.
 * @param {boolean} showButtons Flag whether to show the edit controls. Defaults to false.
 * @param {(id: string, name: string) => void} handleDelete Handler fn for deleting a skill. Id should identify the skill to be deleted and name should identify the property name (skills/requiredSkills)
 * @param {(e: React.BaseSyntheticEvent) => void} handleAdd Handler fn for adding a skill.
 * @param {string} name Name of the property the handlers should target.
 * @returns JSX.Element
 */
export default function EditableSkillList({
  skills,
  showButtons = false,
  handlers: { handleAdd = () => undefined, handleDelete = () => undefined },
  name,
}: EditableSkillList): JSX.Element {
  const [skillOptions, setSkillOptions] = useState<
    (SkillType & { inputValue?: string })[]
  >([]) // state for skill options + extension for completely new skills
  const [value, setValue] = useState<string | null>(null) // state for new skill values

  useEffect(() => {
    // Get data from backend server at run-time (only if edit buttons are supposed to be visible)
    showButtons && getSkills().then(skills => setSkillOptions(skills))
  }, [])

  // Change handler for the autocomplete input
  const handleChange = (
    // eslint-disable-next-line @typescript-eslint/ban-types
    event: React.ChangeEvent<{}>,
    newValue: string | ((SkillType & { inputValue?: string }) | null)
  ) => {
    // On enter or select of a completely new input
    if (newValue && (typeof newValue === "string" || newValue.inputValue)) {
      // Grab values for the new skill from the input / selected new "Add xxx" value
      const newSkill = {
        id: undefined,
        skillName:
          typeof newValue === "string" ? newValue : newValue.inputValue,
      }
      // Create new skill into backend
      createSkill(newSkill).then((id: string) => {
        // Override target name/value in the event with name received in props and the whole Skill object
        handleAdd({
          ...event,
          target: {
            ...event.target,
            name: name,
            value: { ...newSkill, id: id },
          },
        })
      })
    }
    // On select of an existing skill
    else if (newValue && (newValue as SkillType).skillName) {
      // Override target name/value in the event with name received in props and the whole Skill object
      handleAdd({
        ...event,
        target: { ...event.target, name: name, value: newValue },
      })
    }
    setValue(null)
  }

  // Autocomplete filter handler
  const filterOptions = (
    options: typeof skillOptions,
    params: FilterOptionsState<SkillType & { inputValue?: string }>
  ) => {
    const filtered = filter(options, params)

    // Suggest the creation of a new value (if it's not already available)
    if (
      params.inputValue !== "" &&
      !options
        .map(o => o.skillName.toLocaleLowerCase())
        .includes(params.inputValue.toLowerCase())
    ) {
      filtered.push({
        id: "",
        inputValue: params.inputValue,
        skillName: `Add "${params.inputValue}"`,
      })
    }

    return filtered
  }

  // Autocomplete option label handler
  const getOptionLabel = (option: SkillType & { inputValue?: string }) => {
    // Value selected with enter, right from the input
    if (typeof option === "string") {
      return option
    }
    // Add "xxx" option created dynamically
    if (option.inputValue) {
      return option.inputValue
    }
    // Regular option
    return option.skillName
  }

  return (
    <>
      {skills.length ? (
        <ul className="editable-profile-property">
          {skills &&
            skills.map(s => (
              <li key={s.id}>
                <div style={{ position: "relative" }}>
                  {s.skillName}
                  {showButtons && (
                    <DeleteIcon
                      className="button"
                      onClick={() => handleDelete(s.id, name)}
                    />
                  )}
                </div>
              </li>
            ))}
        </ul>
      ) : (
        <ul>
          <li>None</li>
        </ul>
      )}
      {showButtons && (
        <Autocomplete
          style={{ borderRadius: "4px" }}
          value={value}
          onChange={handleChange}
          filterOptions={filterOptions}
          selectOnFocus
          clearOnBlur
          handleHomeEndKeys
          options={skillOptions}
          getOptionLabel={getOptionLabel}
          renderOption={option => option.skillName}
          freeSolo
          renderInput={params => (
            <TextField
              {...params}
              label="Add a new skill..."
              variant="outlined"
              style={{ backgroundColor: "white" }}
            />
          )}
        />
      )}
    </>
  )
}
