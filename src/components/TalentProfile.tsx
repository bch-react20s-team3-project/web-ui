import React, { useState } from "react"

import Avatar from "./Avatar"
import EditableProfileProperty from "./EditableProfileProperty"
import EditableSkillList from "./EditableSkillList"
import * as handlers from "../services/componentStateUpdateHandlers"
import {
  updateTalentProfile,
  updateUser,
} from "../services/dataMutationHandlers"

interface TalentProfile {
  data: TalentProfileType
  editable?: boolean
}

/**
 * @param {TalentProfileComponentType} data Talent profile data to use
 * @param {boolean} editable Flag whether to show edit buttons.
 * @returns JSX.Element
 */
export default function TalentProfile({
  data: propData,
  editable = false,
}: TalentProfile): JSX.Element {
  const [data, setData] = useState(propData)
  const [editField, setEditField] = useState("none")

  const profilePropertyState = (
    editState = false,
    showBaseWhileEditing = false,
    showButtons = editable
  ) => {
    return {
      showBaseWhileEditing: showBaseWhileEditing,
      showButtons: showButtons,
      editState: editState,
    }
  }

  const profilePropertyHandlers = (editField: string) => {
    return {
      handleEdit: () => setEditField(editField),
      handleSave: () => setEditField("none"),
    }
  }

  const skillHandlers = {
    handleAdd: (e: React.BaseSyntheticEvent) =>
      handlers.addSkill(data, e, setData),
    handleDelete: (id: string, name: string) => {
      handlers.removeSkill(data, { id, name }, setData)
    },
  }

  const onImgChange = (e: React.BaseSyntheticEvent) =>
    handlers.updateImg(data, e, setData)

  const onOwnerChange = (e: React.BaseSyntheticEvent) =>
    handlers.updateOwner(data, e, setData)

  const onTextChange = (e: React.BaseSyntheticEvent) =>
    handlers.updateText(data, e, setData)

  // Post data mutations to Strapi
  const handlePublish = () => {
    //TODO conditions to update only if there are actual changes
    updateTalentProfile(data).catch(console.log)
    updateUser(data.owner).catch(console.log)
  }

  return (
    <>
      <section>
        {/* user img */}
        <EditableProfileProperty
          baseElement={<Avatar size="large" src={String(data.img)} />}
          editElement={<input name="img" type="file" onChange={onImgChange} />}
          state={profilePropertyState(editField === "img", true)}
          handlers={profilePropertyHandlers("img")}
        />

        {/* user name */}
        <EditableProfileProperty
          baseElement={data.owner?.username}
          editElement={
            <input
              name="username"
              type="text"
              value={data.owner?.username}
              onChange={onOwnerChange}
            />
          }
          state={profilePropertyState(editField === "username")}
          handlers={profilePropertyHandlers("username")}
        />

        {/* user email */}
        {editable && (
          <EditableProfileProperty
            baseElement={data.owner?.email || ""}
            editElement={
              <input
                name="email"
                type="text"
                value={data.owner?.email}
                onChange={onOwnerChange}
              />
            }
            state={profilePropertyState(editField === "email")}
            handlers={profilePropertyHandlers("email")}
          />
        )}

        {/* user.shortDescription */}
        <EditableProfileProperty
          baseElement={data.shortDescription || ""}
          editElement={
            <input
              name="shortDescription"
              type="text"
              value={data.shortDescription}
              onChange={onTextChange}
            />
          }
          state={profilePropertyState(editField === "shortDescription")}
          handlers={profilePropertyHandlers("shortDescription")}
        />

        {/* user.location */}
        <EditableProfileProperty
          baseElement={data.location || ""}
          editElement={
            <input
              name="location"
              type="text"
              value={data.location}
              onChange={onTextChange}
            />
          }
          state={profilePropertyState(editField === "location")}
          handlers={profilePropertyHandlers("location")}
        />
      </section>

      {/* user skills */}
      <section>
        <b>Skills</b>
        <EditableSkillList
          skills={data.skills || []}
          showButtons={editable}
          handlers={skillHandlers}
          name="skills"
        />
      </section>

      {/* user description */}
      <section>
        <b>Description</b>
        <EditableProfileProperty
          baseElement={data.description || ""}
          editElement={
            <textarea
              name="description"
              value={data.description}
              onChange={onTextChange}
            />
          }
          state={profilePropertyState(editField === "description")}
          handlers={profilePropertyHandlers("description")}
        />
      </section>

      <section>
        {/* user job type interest */}
        <EditableProfileProperty
          baseElement={`Looking for: ${data.jobTypeInterest} work`}
          editElement={
            <>
              Looking for:
              <select name="jobTypeInterest" onChange={onTextChange}>
                <option>fulltime</option>
                <option>parttime</option>
                <option>freelance</option>
              </select>
            </>
          }
          state={profilePropertyState(editField === "jobTypeInterest")}
          handlers={profilePropertyHandlers("jobTypeInterest")}
        />

        {/* user work experience */}
        <EditableProfileProperty
          baseElement={"Experience: " + data.workExperience}
          editElement={
            <>
              Experience:{" "}
              <input
                name="workExperience"
                type="number"
                value={data.workExperience}
                onChange={onTextChange}
              />
            </>
          }
          state={profilePropertyState(editField === "workExperience")}
          handlers={profilePropertyHandlers("workExperience")}
        />
      </section>

      {editable && (
        <h3
          onClick={handlePublish}
          style={{ display: "flex", justifyContent: "space-between" }}
        >
          <span>Save changes</span>
          <span>{">"}</span>
        </h3>
      )}
    </>
  )
}
