import React, { useState } from "react"
import { navigate } from "gatsby"

import Avatar from "./Avatar"
import EditableProfileProperty from "./EditableProfileProperty"
import EditableSkillList from "./EditableSkillList"
import * as handlers from "../services/componentStateUpdateHandlers"
import {
  createPositionProfile,
  updatePositionProfile,
  deletePositionProfile,
} from "../services/dataMutationHandlers"

interface PositionProfile {
  data: PositionProfileType
  editable?: boolean
}

/**
 * @param {PositionProfileType} data Position profile data to use.
 * @param {boolean} editable Flag whether to show edit buttons.
 * @returns JSX.Element
 */
export default function PositionProfile({
  data: propData,
  editable,
}: PositionProfile): JSX.Element {
  const [data, setData] = useState(propData)
  const [editField, setEditField] = useState("none")

  const profilePropertyState = (
    editState = false,
    showBaseWhileEditing = false,
    showButtons = editable
  ) => {
    return {
      showBaseWhileEditing: showBaseWhileEditing,
      showButtons: showButtons,
      editState: editState,
    }
  }

  const profilePropertyHandlers = (editField: string) => {
    return {
      handleEdit: () => setEditField(editField),
      handleSave: () => setEditField("none"),
    }
  }

  const skillHandlers = {
    handleAdd: (e: React.BaseSyntheticEvent) =>
      handlers.addSkill(data, e, setData),
    handleDelete: (id: string, name: string) => {
      handlers.removeSkill(data, { id, name }, setData)
    },
  }

  const onImgChange = (e: React.BaseSyntheticEvent) =>
    handlers.updateImg(data, e, setData)

  const onOwnerChange = (e: React.BaseSyntheticEvent) =>
    handlers.updateOwner(data, e, setData)

  const onTextChange = (e: React.BaseSyntheticEvent) =>
    handlers.updateText(data, e, setData)

  const handlePublish = () => {
    // Create if we don't have an existing position id, otherwise update
    //TODO conditions to update only if there are actual changes
    !data.id
      ? createPositionProfile(data).catch(console.log)
      : updatePositionProfile(data).catch(console.log)

    navigate(`/profile`)
    //TODO pass some sort of state prop to profile page so it can render the position list correctly (new item is being added) before the re-deployment has gone thru?
  }

  const handleDelete = () => {
    deletePositionProfile(data.id)
    navigate(`/profile`)
    //TODO pass some sort of state prop to profile page so it can render the position list correctly (one item is being deleted) before the re-deployment has gone thru?
  }

  return (
    <>
      <section>
        {/* = position.img = */}
        <EditableProfileProperty
          baseElement={<Avatar size="large" src={String(data.img)} />}
          editElement={<input name="img" type="file" onChange={onImgChange} />}
          state={profilePropertyState(editField === "img", true)}
          handlers={profilePropertyHandlers("img")}
        />

        {/* position.companyName */}
        <EditableProfileProperty
          baseElement={data.owner?.companyName || ""}
          editElement={
            <input
              name="companyName"
              type="text"
              value={data.owner?.companyName}
              onChange={onOwnerChange}
            />
          }
          state={profilePropertyState(false, false, false)}
          handlers={profilePropertyHandlers("companyName")}
        />

        {/* = position.positionName =  */}
        <EditableProfileProperty
          baseElement={data.name || ""}
          editElement={
            <input
              name="name"
              type="text"
              value={data.name}
              onChange={onTextChange}
            />
          }
          state={profilePropertyState(editField === "name")}
          handlers={profilePropertyHandlers("name")}
        />

        {/* = position.location =  */}
        <EditableProfileProperty
          baseElement={data.location || ""}
          editElement={
            <input
              name="location"
              type="text"
              value={data.location}
              onChange={onTextChange}
            />
          }
          state={profilePropertyState(editField === "location")}
          handlers={profilePropertyHandlers("location")}
        />

        {/* = position.jobType =  */}
        <EditableProfileProperty
          baseElement={`Job type: ${data.jobType}`}
          editElement={
            <>
              Job type:
              <select name="jobType" onChange={onTextChange}>
                <option>fulltime</option>
                <option>parttime</option>
                <option>freelance</option>
              </select>
            </>
          }
          state={profilePropertyState(editField === "jobType")}
          handlers={profilePropertyHandlers("jobType")}
        />

        {/* = position.requiredExperience =  */}
        <EditableProfileProperty
          baseElement={`Required experience: ${data.requiredExperience} years`}
          editElement={
            <>
              Required experience:{" "}
              <input
                name="requiredExperience"
                type="number"
                value={data.requiredExperience}
                onChange={onTextChange}
              />
            </>
          }
          state={profilePropertyState(editField === "requiredExperience")}
          handlers={profilePropertyHandlers("requiredExperience")}
        />
      </section>

      <section>
        <b>Required skills</b>
        {/* = position.requiredSkills =  */}
        <EditableSkillList
          skills={data.requiredSkills || []}
          showButtons={editable}
          handlers={skillHandlers}
          name="requiredSkills"
        />
      </section>

      {/* = position.description =  */}
      <section>
        <b>Description</b>
        <EditableProfileProperty
          baseElement={data.description || ""}
          editElement={
            <textarea
              name="description"
              value={data.description}
              onChange={onTextChange}
            />
          }
          state={profilePropertyState(editField === "description")}
          handlers={profilePropertyHandlers("description")}
        />
      </section>

      {editable && (
        <div className="position-profile-buttons">
          <button onClick={() => navigate(`/profile`)}>Cancel</button>
          <button onClick={handlePublish}>Publish</button>
          {data.id && <button onClick={handleDelete}>Delete</button>}
        </div>
      )}
    </>
  )
}
