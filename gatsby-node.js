exports.createPages = async ({ graphql, actions }) => {
  // Get profiles from Strapi
  const data = await graphql(`
    query {
      allStrapiTalentProfiles {
        nodes {
          description
          owner {
            id
            email
            username
          }
          jobTypeInterest
          strapiId
          workExperience
          skills {
            id
            skillName
          }
          location
          shortDescription
          img
          draft
        }
      }
      allStrapiPositionProfiles {
        nodes {
          description
          jobType
          location
          owner {
            id
            companyName
          }
          strapiId
          name
          requiredExperience
          requiredSkills {
            id
            skillName
          }
          img
        }
      }
      allStrapiEmployerProfiles {
        nodes {
          strapiId
          img
          owners {
            id
            email
            username
          }
          positions {
            id
            name
            location
            jobType
          }
          companyName
        }
      }
    }
  `)

  const employerProfiles = [...data.data.allStrapiEmployerProfiles.nodes]
  const positionProfiles = [...data.data.allStrapiPositionProfiles.nodes]
  const talentProfiles = [...data.data.allStrapiTalentProfiles.nodes]
  const allProfiles = [
    ...employerProfiles,
    ...positionProfiles,
    ...talentProfiles,
  ]

  // Rename strapiId -> id
  allProfiles.forEach(p => {
    p.id = p.strapiId
    delete p.strapiId
  })

  // Create pages out of the profile data
  const { createPage } = actions

  // Create /home/{id} page for all talent and position profiles (except drafts)
  const homePages = [...talentProfiles, ...positionProfiles]
  homePages.forEach(page => {
    if (!page.draft)
      createPage({
        path: `/home/${page.id}`,
        component: require.resolve("./src/templates/home/id.tsx"),
        context: {
          id: page.id,
          data: page,
        },
      })
  })

  // Create /profile/{id} page for all talent and employer profiles
  const profilePages = [...talentProfiles, ...employerProfiles]
  profilePages.forEach(page => {
    createPage({
      path: `/profile/${page.id}`,
      component: require.resolve("./src/templates/profile/id.tsx"),
      context: {
        id: page.id,
        data: page,
      },
    })
  })

  // Create /position/{id} page for all position profiles
  positionProfiles.forEach(page => {
    createPage({
      path: `/position/${page.id}`,
      component: require.resolve("./src/templates/position/id.tsx"),
      context: {
        id: page.id,
        data: page,
      },
    })
  })
}
