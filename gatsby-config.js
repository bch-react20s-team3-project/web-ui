require("dotenv").config()
module.exports = {
  siteMetadata: {
    title: `Gatsby Default Starter`,
    description: `Kick off your next, great Gatsby project with this default starter. This barebones starter ships with the main Gatsby configuration files you might need.`,
    author: `@gatsbyjs`,
    strapiUrl: process.env.GATSBY_STRAPI_URL,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-image`,
    `gatsby-transformer-json`, // For sourcing test-data JSONs FIXME: remove once not needed
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `test-data`,
        path: `${__dirname}/src/test-data/`,
      },
    },
    // ================================================================
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `jobvious-web-ui`,
        short_name: `Jobvious`,
        start_url: `/get-started`,
        background_color: `#2D302D`,
        theme_color: `#EF7066`,
        display: `standalone`,
        icon: `src/images/gatsby-icon.png`, // This path is relative to the root of the site.
      },
    },
    `gatsby-plugin-gatsby-cloud`,
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
    {
      resolve: `gatsby-source-strapi`,
      options: {
        apiURL: process.env.GATSBY_STRAPI_URL || `http://localhost:1337`,
        queryLimit: 100,
        contentTypes: [
          `talent-profiles`,
          `position-profiles`,
          `employer-profiles`,
        ], //TODO add the new phrases collection
        loginData: {
          identifier: process.env.STRAPI_USER,
          password: process.env.STRAPI_PASSWORD,
        },
      },
    },
    `gatsby-plugin-sass`,
  ],
}
