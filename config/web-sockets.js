import io from "socket.io-client"

export const socket = io(process.env.GATSBY_STRAPI_URL)
