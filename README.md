# Jobvious web-ui

## Development
To set up your development environment, please do the following:
- `npm install` to install all the required dependencies
- `npx husky install` to install local husky dependencies
- `npm start` to start the app in development mode
  - Note that there's also a ready made vscode launch configuration for the same. You can just press F5 to start the server in development mode.
  
Please also take not of the most important env variables for connecting to the backend server:
```
GATSBY_STRAPI_URL=http://localhost:1337
STRAPI_USER=your@email.here
STRAPI_PASSWORD=your-password
ENABLE_GATSBY_REFRESH_ENDPOINT=true
```

Note that some backend must be reachable for development, data is being sourced into Gatsby at the build step.

## Project structure
The app has been bootstrapped with Gatsby and Sass.

### Gatsby
Source code is structured using the default Gatsby conventions, `src` has the React sources with:
- `src/components` for individual (functional) React components
- `src/pages` for Gatsby page components
- `src/templates` for Gatsby (page) template components
  - Files are structured per path, e.g. /home/id.tsx could build into a page at /home/1a2b3c4d5e6f7
- `src/services` for common utility components, e.g. for React component state management and run-time query/mutation of backend data

There's also `src/test-data` for static test data JSONs that can be sourced into the Gatsby GraphQL data layer.

Everything under `src` is (and should be) in TypeScript - relevant declarations and type guard utilities can be found in the root:
- `type-guards.ts`
- `typings.d.ts`

Also `gatsby-node.js` is used to source in data and build static components using the template components at the build time.

### Styling with Sass
#### Architecture
Styling is produced using Sass. All the styling will be in scss folder which contains seven different folders:
- abstracts
- base
- layout
- pages
- themes
- vendors
Also there will be main.scss file in the root of scss - folder.

More info about these folders and their contents can be found from:
<https://sass-guidelin.es/#the-7-1-pattern>

#### Adding styling
To add your own styles add your scss file in to correct folder (in most of the cases components, layout or pages folder). Then import the file in the main.scss file using *@use* -command.

```scss
@use './<yourfolder>/<yourfile.scss>';
```
**All the files added to main.scss has to always start with _**

For example correct filename is: `_yourfile.scss`
#### 
In order to use already declared variables (colors, measures, mixins etc.)
you need to import them in to your working file using *@use*-command. Sass enables you to add your own namespaces while using *@use* but in the name of clarity we should not use them. Correct way to import files is:
```scss
@use './variables/colors' as *;
```
This removes local namespaces and you can use imported variables as they are declared.

**Do not clear namespace when importing in to main.scss file**

**Note that path imports/references (e.g. to image files) must be relative to the main.scss, even in the fragment files (the ones starting with `_...`)!**
